<?php
// Parent Segi Empat
class SegiEmpat {
    private $panjang;
    private $lebar;

    // Method Set Panjang
    public function setPanjang($panjang)
    {
        $this->panjang=$panjang;
    }

    // Method Get Panjang
    public function getPanjang()
    {
        return $this->panjang;
    }

    // Method Set Lebar
    public function setLebar($lebar)
    {
        $this->lebar=$lebar;
    }

    // Method Get Lebar
    public function getLebar()
    {
        return $this->lebar;
    }

    // Method Luas Alas
    public function luas()
    {
        return $this->getPanjang() * $this->getLebar(); // Rumus La = p * l
    }
}

// Child Balok
class Balok extends SegiEmpat {
    private $tinggi;

    // Method Set Tinggi
    public function setTinggi($tinggi) 
    {
        $this->tinggi=$tinggi;
    }

    // Method Get Tinggi
    public function getTinggi()
    {
        return $this->tinggi;
    }

    // Method Volume
    public function volume() 
    {
        return $this->luas() * $this->getTinggi(); // Rumus V = p * l * t
    }
}

// Child Kubus
class Kubus extends SegiEmpat {
    private $sisi;

    // Method Set Sisi
    public function setSisi($sisi) 
    {
        $this->sisi=$sisi;
    }

    // Method Get Sisi
    public function getSisi()
    {
        return $this->sisi;
    }  
    // Method Volume
    public function volume()
    {
        return $this->luas() * $this->getSisi(); //Rumus V = s * s * s
    }
}

// Balok
echo "Balok <br>";
$balok = new Balok();

// Set Nilai
$balok->setPanjang(4);
$balok->setLebar(5);
$balok->setTinggi(6);

// Get Nilai
$balok->getPanjang();
$balok->getLebar();
$balok->getTinggi();

// Tampil Luas Alas
echo "Luas Alas = "; 
echo $balok->luas()."<br>";

// Tampil Volume
echo "Volume = ";
echo $balok->volume()."<br>"; 

########## Break Pemisah ##########
echo "<br>";

// Kubus
echo "Kubus <br>";
$kubus = new Kubus();

// Set Nilai, karena ini kubus, menggunakan sisi, maka nilai harus sama
$kubus->setPanjang(4); // Anggap saja ini sisi (s)
$kubus->setLebar(4); // Anggap saja ini sisi (s)
$kubus->setSisi(4);

// Get Nilai
$kubus->getPanjang();
$kubus->getLebar();
$kubus->getSisi();

// Tampil Luas Permukaan
echo "Luas Permukaan = "; 
echo $kubus->luas()."<br>";

// Tampil Volume
echo "Volume = ";
echo $kubus->volume()."<br>"; 
?>